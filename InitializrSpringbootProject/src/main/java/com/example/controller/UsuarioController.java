
package com.example.controller;

import com.example.dao.UsuariotRepository;
import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.model.Usuario;
import com.example.service.UsuarioService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andres Escobar
 */
@RestController
public class UsuarioController {

    @RequestMapping("/home")
    public String home() {

        return "Web Service SpringBoot";
    }

    
    @Autowired
    UsuarioService contactService;
    @Autowired
    UsuariotRepository noteRepository;

    /*Inserta nuevo usuario*/
    @RequestMapping(value = "/usuarios/crea", method = RequestMethod.POST)
    public Usuario updateOrSave(@RequestBody Usuario contact) {

        return contactService.save(contact); 
    }

    /*Actualiza usuario por id*/
    //@PutMapping("/usuarios/{id}")
    @RequestMapping(value = "/usuarios/update/{id}", method = RequestMethod.PUT)
    public Usuario updateUser(@PathVariable(value = "id") Long noteId,
            @Valid @RequestBody Usuario usrDetails) {

        Usuario usr = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("usuario", "id", noteId));

        usr.setEmail(usrDetails.getEmail());
        usr.setFirstName(usrDetails.getFirstName());
        usr.setLastName(usrDetails.getLastName());
        usr.setPhoneNumber(usrDetails.getPhoneNumber());

        Usuario updatedNote = noteRepository.save(usr);
        return updatedNote;
    }

    /*Elimina usuario por id*/
    //@DeleteMapping("/usuarios/{id}")
    @RequestMapping(value = "/usuarios/elimina/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long noteId) {
        Usuario us = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("usuario", "id", noteId));

        noteRepository.delete(us);

        // return ResponseEntity.ok().build();
        return ResponseEntity.ok(us);
    }

    /*Obtiene Lista de todos los usuarios*/
    //@GetMapping("/usuarios")
    @RequestMapping(value = "/usuarios/todos", method = RequestMethod.GET)
    public List<Usuario> getAllNotes() {
        return contactService.findAll();
    }

    /*Obtiene usuario especifico por id*/
    //@GetMapping("/usuarios/{id}")
    @RequestMapping(value = "/usuarios/porid/{id}", method = RequestMethod.GET)
    public Usuario getNoteById(@PathVariable(value = "id") Long noteId) {
        return noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("usuario", "id", noteId));
    }

    
    /**/
}
