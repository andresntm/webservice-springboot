
package com.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;
 
import com.example.model.Usuario;
import java.util.List;
 
/**
 *
 * @author Andres Escobar
 */
public interface UsuariotRepository extends JpaRepository<Usuario, Long> {
  
    
}