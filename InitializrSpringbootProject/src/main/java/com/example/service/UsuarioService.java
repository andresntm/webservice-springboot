/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.dao.UsuariotRepository;
import java.util.List;

/**
 *
 * @author Andres Escobar
 */

@Service
public class UsuarioService {

    @Autowired
    UsuariotRepository dao;
   
    public Usuario save(Usuario contact){
        return dao.saveAndFlush(contact);
    }
    
    
    
    public List<Usuario> findAll() {

        return   dao.findAll();
    }
}